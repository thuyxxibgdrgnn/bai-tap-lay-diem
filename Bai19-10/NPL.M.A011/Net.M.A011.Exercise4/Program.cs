﻿// See https://aka.ms/new-console-template for more information
using Net.M.A011.Exercise4;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int[] array = new int[] { 3, 2, 5, 6, 1, 7, 7, 5, 2 };
int elementOfOrder2 = Ex.ElementOfOrder2(array);
Console.WriteLine("Số lớn thứ 2 trong mảng: " + elementOfOrder2);

int elementOfOrder = Ex.ElementOfOrder(array, 2);
Console.WriteLine("Phần tử orderLargest lớn nhất trong mảng: " + elementOfOrder);

int elementOfOrder3 = Ex.ElementOfOrder(array, 20);
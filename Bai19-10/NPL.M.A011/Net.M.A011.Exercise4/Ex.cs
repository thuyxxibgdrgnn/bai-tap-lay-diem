﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise4
{
    public static class Ex
    {
        public static int ElementOfOrder2(int[] array)
        {
            int count = 0;
            int max = int.MinValue;
            int max2 = int.MinValue;
            for (int i = 0; i < array.Length; ++i)
            {
                if (array[i] > max)
                {
                    count++;
                    max2 = max;
                    max = array[i];
                }
            }
            if (count < 2)
            {
                return max;
            }
            return max2;
        }
        public static T ElementOfOrder<T>(T[] array, int orderLargest) where T : IComparable<T>
        {
            T[] arr1 = array.Distinct().ToArray();
            for (int i = 0; i < arr1.Length; ++i)
            {
                for (int j = i; j < arr1.Length; ++j)
                {
                    if (arr1[i].CompareTo(arr1[j]) > 0)
                    {
                        (arr1[i], arr1[j]) = (arr1[j], arr1[i]);
                    }
                }
            }
            if (orderLargest < arr1.Length)
            {
                return arr1[arr1.Length - orderLargest];

            }
            else throw new Exception();

        }

    }

   

}


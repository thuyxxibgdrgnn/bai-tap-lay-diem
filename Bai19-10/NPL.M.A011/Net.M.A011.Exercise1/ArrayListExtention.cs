﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise1
{
    public static class ArrayListExtention
    {
        public static int CountInt(ArrayList arr)
        {
            int count = 0;
            foreach (var item in arr)
            {
                if (item is int)
                {
                    count++;
                }
            }
            return count;   
        }
        public static int CountOf(ArrayList array, Type dataType)
        {
            int count = 0;
            foreach (var item in array)
            {
                if (dataType.IsInstanceOfType(item))
                {
                    count++;
                }
            }
            return count;
        }
       

        public static int CountOf<T>( ArrayList array)
        {
            int count = 0;
            foreach (var item in array)
            {
                if (item is T)
                {
                    count++;
                }
            }
            return count;
        }

        public static T MaxOf<T>( ArrayList array)
        {
            if (typeof(T).IsNumericType())
            {
                var numericValues = array.OfType<T>().ToList();
                if (numericValues.Count == 0)
                {
                    throw new InvalidOperationException("Mảng ArrayList không chứa phần tử nào có kiểu dữ liệu " + typeof(T).Name);
                }
                return numericValues.Max();
            }
            else
            {
                throw new InvalidOperationException("Kiểu dữ liệu " + typeof(T).Name + " không phải là kiểu số.");
            }
        }

        private static bool IsNumericType(this Type type)
        {
            return type == typeof(int)
                || type == typeof(long) 
                || type == typeof(float) 
                || type == typeof(double) 
                || type == typeof(decimal);
        }





    }
}

﻿// See https://aka.ms/new-console-template for more information
using Net.M.A011.Exercise1;
using System.Collections;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
ArrayList arrayList = new ArrayList
{
    "Hieu", "Hoang", 1, 2, "Trong", 3.9d
};

int countInt = ArrayListExtention.CountInt(arrayList);
int countOf = ArrayListExtention.CountOf(arrayList, typeof(string));
int countOfT = ArrayListExtention.CountOf<string>(arrayList);
int maxT= ArrayListExtention.MaxOf<int>(arrayList);

Console.WriteLine("Số phần tử kiểu int: " + countInt);
Console.WriteLine("Số phần tử kiểu dữ liệu nhập vào: "+ countOf);
Console.WriteLine("Số phần tử trong mảng có kiểu dữ liệu T: " + countOfT);
Console.WriteLine("Phần tử max nếu T là kiểu dữ liệu số: " +maxT);

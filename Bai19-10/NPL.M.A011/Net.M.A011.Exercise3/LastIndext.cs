﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise3
{
    internal class LastIndext
    {
        public static int LastIndexOf<T>( T[] array, T elementValue)
        { 
            for(int i = array.Length-1; i >=0 ; i--)
            {
                if (elementValue.Equals(array[i]))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}

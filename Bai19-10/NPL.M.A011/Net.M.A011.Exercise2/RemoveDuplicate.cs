﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise2
{
    public static class Remove
    {
        public static int[] RemoveDuplicate(int[] arr)
        {
            List<int> list = new List<int>();
            foreach (int i in arr)
            {
                if (!list.Contains(i))
                {
                    list.Add(i);
                }
            }
            return list.ToArray();
        }
        public static T[] RemoveDuplicate<T>( T[] arr)
        {
            List<T> list = new List<T>();
            foreach (T i in arr)
            {
                if (!list.Contains(i))
                {
                    list.Add(i);
                }
            }
            return list.ToArray();
        }
    }
}

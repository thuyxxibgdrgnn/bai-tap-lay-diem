﻿// See https://aka.ms/new-console-template for more information
using Net.M.A011.Exercise2;
using System.Collections;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int[] array = new int[] { 1, 2, 3, 3, 5, 6, 5, 2 };
int[] arrDistinct = Remove.RemoveDuplicate(array);
Console.WriteLine("arrDistinct: ");
foreach (int item in arrDistinct)
{
    Console.WriteLine(item);
}

string[] arrayString = new string[]
{
    "Hung", "Vu", "Van", "Hung", "Quang", "Huy", "Vu"
};
string[] arrStringDistinct = Remove.RemoveDuplicate(arrayString);
Console.WriteLine("arrStringDistinct: ");
foreach (string item in arrStringDistinct)
{
    Console.WriteLine(item);
}

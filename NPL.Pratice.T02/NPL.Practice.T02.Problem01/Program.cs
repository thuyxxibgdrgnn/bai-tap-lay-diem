﻿// See https://aka.ms/new-console-template for more information
using static System.Net.Mime.MediaTypeNames;
using System;

static  string GetArticleSummary(string content, int maxLength)
{
    if (content.Length > maxLength)
    {
        if (content[maxLength].Equals(" "))
        {
            return content.Substring(0, maxLength) + "...";
        }
        else
        {
            int index = 0;
            for (int i = maxLength; i >= 0; --i)
            {
                if (content[i].Equals(' '))
                {
                    index = i;
                    break;
                }
            }
            return content.Substring(0, index) + "...";
        }

    }
    else
    {
        return content;
    }

}
Console.Write("Content of article: ");
string str = Console.ReadLine();
Console.WriteLine("Max length of summary: ");
int len = int.Parse(Console.ReadLine());
Console.WriteLine(GetArticleSummary(str, len));










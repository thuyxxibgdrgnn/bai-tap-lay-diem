﻿// See https://aka.ms/new-console-template for more information
 static int FindMaxSubArray(int[] inputArray, int subLength)
{
    if (inputArray == null || inputArray.Length < subLength)
    {
        throw new ArgumentException("Invalid input array or subLength");
    }

    int maxSum = int.MinValue;
    int currentSum = 0;

    for (int i = 0; i < subLength; i++)
    {
        currentSum += inputArray[i];
    }

    maxSum = currentSum;

    for (int i = subLength; i < inputArray.Length; i++)
    {
        currentSum += inputArray[i] - inputArray[i - subLength];
        maxSum = Math.Max(maxSum, currentSum);
    }

    return maxSum;

}
Console.Write("Input length of array: ");
int n = int.Parse(Console.ReadLine());
Console.WriteLine("Input array: ");
int[] array = new int[n];
for (int i = 0; i < n; ++i)
{
    Console.Write("Array["+i+"]: ");
    array[i] = int.Parse(Console.ReadLine());
}
Console.Write("Input Sub Length: ");
int subLen = int.Parse(Console.ReadLine());
int maxSubArray = FindMaxSubArray(array, subLen);
Console.WriteLine("Output: "+ maxSubArray);

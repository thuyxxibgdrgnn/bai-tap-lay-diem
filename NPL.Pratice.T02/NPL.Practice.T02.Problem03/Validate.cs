﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Validate
    {
        public int checkIdExist(List<Student> danhSach, string mess)
        {
            Console.Write(mess);
            int id = 0;
            do
            {
                try
                {
                    id = int.Parse(Console.ReadLine());

                    bool isIdDuplicate = false;
                    if (id < 0)
                    {
                        throw new Exception();

                    }
                    else
                    {
                        foreach (Student student in danhSach)
                        {
                            if (id.Equals(student.Id))
                            {
                                isIdDuplicate = true;
                                break;
                            }
                        }
                        if (isIdDuplicate)
                        {
                            Console.WriteLine("ID is already exist!");
                            Console.Write(mess);
                        }
                        else
                        {
                            break;
                        }
                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Enter integer ");
                }

            } while (true);
            return id;
        }
        

        public double CheckDouble(string mess)
        {
            double input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());

                    if (input < 0)
                    {
                        throw new Exception();
                    }

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid, please re-enter: ");
                }
            } while (true);

            return input;
        }
        public string CheckString(string message, string pattern)
        {
            string input = string.Empty;
            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Invalid");
                    continue;
                }
                else
                {
                    break;
                }
            }

            return input;
        }
        public int CheckInt(string mess)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());

                    if (input < 0)
                    {
                        throw new Exception();
                    }

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Enter Integer ");
                }
            } while (true);

            return input;
        }
        public DateTime CheckDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string intput = Console.ReadLine();
                try
                {
                    DateTime date = DateTime.ParseExact(intput, "dd/MM/yyyy", null);
                    return date;
                }
                catch
                {
                    Console.WriteLine("Invalid format date: ");
                }
            }

        }

        
        public int checkIntLimit(string mess, int a, int b)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());
                    if (input >= a && input <= b)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Must be in " + a + "-" + b);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Option invalid");
                }
            } while (true);
            return input;
        }
        public bool CheckYesNo(string message)
        {
            while (true)
            {
                Console.Write(message);
                string input = Console.ReadLine();
                if (input.ToLower().Equals("y"))
                {
                    return true;
                }
                else if (input.ToLower().Equals("n"))
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Enter Y/y or N/n!: ");
                }
            }
        }
    }
}

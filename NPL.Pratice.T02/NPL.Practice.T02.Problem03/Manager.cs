﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Manager
    {
        List<Student> studentList = new List<Student>();
        Validate validate = new Validate();
        public void AddStudent()
        {
            do
            {
                int ID = validate.checkIdExist(studentList, "Enter ID: ");
                string name = validate.CheckString("Enter Name: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                DateTime startDate = validate.CheckDate("Start Date: ");
                decimal sqlMark = validate.checkIntLimit("Sql Mark: ", 0, 10);
                decimal CsharpMark = validate.checkIntLimit("C# Mark: ", 0, 10);
                decimal DsaMark = validate.checkIntLimit("Dsa Mark: ", 0, 10);
                Student student = new Student(ID, name, startDate, sqlMark, CsharpMark, DsaMark);
                student.Graduate();
                studentList.Add(student);

            } while (validate.CheckYesNo("Do you want add more?(Y/N): "));

        }
        public void Display()
        {
            
            foreach (Student student in studentList)
            {
                Console.WriteLine(student.GetCertificate()); 
            }

        }
        public void InputData()
        {
            studentList.Add(new Student(1, "Đỗ Thanh Thủy", DateTime.Now, 7, 8, 9));
            studentList.Add(new Student(2, "Nguyễn Văn Minh", DateTime.Now, 9, 5, 7));
            studentList.Add(new Student(3, "Hoàng Nam", DateTime.Now, 6, 8, 8));
        }

    }
}

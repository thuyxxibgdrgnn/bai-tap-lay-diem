﻿// See https://aka.ms/new-console-template for more information
using NPL.Practice.T02.Problem03;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Manager manager = new Manager();
manager.InputData();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1.Add Strudent");
    Console.WriteLine("2.Show Certificate");
    Console.WriteLine("0.Exit");
    choice = validate.checkIntLimit("Option: ", 0, 2);
    switch (choice)
    {
        case 1:
            manager.AddStudent();
            break;
        case 2:
            manager.Display();
            break;
       
        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();

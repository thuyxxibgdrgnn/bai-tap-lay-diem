﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeTeacher
{
    internal class Service
    {
        Validate validate = new Validate();
        List<Teacher> giaoVien = new List<Teacher>();
        public void NhapDoiTuong()
        {
            do
            {
                int ID = validate.checkIdExist(giaoVien, "Nhập ID: "); 
                string maGV = validate.CheckString("Nhập mã GV: ", "^[A-Za-z0-9]+$");
                string nganh = validate.CheckString("Nhập ngành dạy: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                Teacher gv = new Teacher(ID, maGV, nganh);
                giaoVien.Add(gv);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -20}{2, -20}",
                                            "ID", "Mã Giáo Viên", "Ngành dạy"));
            foreach (Teacher gv in giaoVien)
            {
                gv.inThongTin();
            }

        }
        public void XuatGiaoVienNganhUDPM()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -20}{2, -20}",
                                            "ID", "Mã Giáo Viên", "Ngành dạy"));
            foreach (var giaoVien in giaoVien)
            {
                if (giaoVien.Nganh == "UDPM")
                {
                    giaoVien.inThongTin();
                }
            }
        }

        public void SapXepTheoNganh()
        {

            for (int i = 0; i < giaoVien.Count - 1; i++)
            {
                for (int j = i + 1; j < giaoVien.Count; j++)
                {
                    if (string.Compare(giaoVien[i].Nganh, giaoVien[j].Nganh) > 0)
                    {
                        var temp = giaoVien[i];
                        giaoVien[i] = giaoVien[j];
                        giaoVien[j] = temp;
                    }
                }
            }
            HienThiDanhSach();
        }
        
        public void NhapDuLieu()
        {
            giaoVien.Add(new Teacher(123, "GV01", "UDPM"));
            giaoVien.Add(new Teacher(234, "GV01", "SE"));
            giaoVien.Add(new Teacher(345, "GV01", "AI"));
            giaoVien.Add(new Teacher(456, "GV01", "UDPM"));
        }

    }
}

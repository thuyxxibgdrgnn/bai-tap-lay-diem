﻿// See https://aka.ms/new-console-template for more information
using DeTeacher;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách giáo viên");
    Console.WriteLine("2. Xuất danh sách giáo viên");
    Console.WriteLine("3. Xuất danh sách giáo viên theo ngành UDPM");
    Console.WriteLine("4. Sắp xếp giáo viên theo ngành");
    Console.WriteLine("0. Thoát");

    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            service.XuatGiaoVienNganhUDPM();
            break;
        case 4:
            service.SapXepTheoNganh();
            break;
       
        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();

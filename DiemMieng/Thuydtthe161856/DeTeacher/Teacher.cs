﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeTeacher
{
    internal class Teacher
    {
        public int ID { get; set; }
        public string MaGV { get; set; }
        public string Nganh { get; set; }

        public Teacher(int id, string maGV, string nganh)
        {
            ID = id;
            MaGV = maGV;
            Nganh = nganh;
        }

        public void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -20}{2, -20}", this.ID, this.MaGV, this.Nganh));
        }
    }
}

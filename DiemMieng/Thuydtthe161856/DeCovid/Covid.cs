﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeCovid
{
    internal class Covid
    {
        
        public string MaCovid {  get; set; }
        public string Ten {  get; set; }
        public int NamPhatHien { get; set; }
        public Covid()
        {
        }

        public Covid(string maCovid, string ten, int namPhatHien)
        {
            MaCovid = maCovid;
            Ten = ten;
            NamPhatHien = namPhatHien;
        }
        public void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -15}", this.MaCovid, this.Ten, this.NamPhatHien));
        }
    }
}

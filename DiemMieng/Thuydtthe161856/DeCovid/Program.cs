﻿// See https://aka.ms/new-console-template for more information
using DeCovid;
using System.ComponentModel.DataAnnotations;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1.Nhập danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xuất các đối tượng có mã bắt đầu bằng chữ “CO”");
    Console.WriteLine("4.Sắp xếp năm phát hiện chủng covid mới theo thứ tự tăng dần");
    Console.WriteLine("0.Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            
            service.XuatDoiTuongCoMaBatDauBangCO();
            break;
        case 4:
            service.SapXepTheoNamPhatHien();
            break;

        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();
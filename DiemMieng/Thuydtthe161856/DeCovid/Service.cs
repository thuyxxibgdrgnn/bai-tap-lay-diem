﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeCovid
{
    internal class Service
    {
        Validate validate = new Validate();
        List<Covid> covid = new List<Covid>();
        public void NhapDoiTuong()
        {
            do
            {
                string ID = validate.checkIdExist(covid, "Nhập mã: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int namPT = validate.CheckInt("Nhập số giờ dạy: ");
                Covid cv = new Covid(ID, ten, namPT);
                covid.Add(cv);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -10}",
                                            "ID", "Tên", "Năm phát triển"));
            foreach (Covid cv in covid)
            {
                cv.inThongTin();
            }

        }
        public void XuatDoiTuongCoMaBatDauBangCO()
        {
            if (covid.Count == 0)
            {
                Console.WriteLine("Không có dữ liệu");
            }
            List<Covid> list = new List<Covid>();
            foreach (Covid cv in covid)
            {
                if (cv.MaCovid.ToUpper().StartsWith("CO")){
                    list.Add(cv);
                }
            }
            if(list.Count == 0)
            {
                Console.WriteLine("Không tìm thấy!");
            }
            else
            {
                Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -15}", "Mã Covid", "Tên","Năm phát triển"));
                foreach(Covid cv in list)
                {
                    cv.inThongTin();
                }

            }

        }
        public void SapXepTheoNamPhatHien()
        {
            for (int i = 0; i < covid.Count - 1; i++)
            {
                for (int j = i + 1; j < covid.Count; j++)
                {
                    if (covid[i].NamPhatHien > covid[j].NamPhatHien)
                    {
                        Covid temp = covid[i];
                        covid[i] = covid[j];
                        covid[j] = temp;
                    }
                }
            }

            foreach (var covid in covid)
            {
                covid.inThongTin();
            }
        }
        




        public void NhapDuLieu()
        {
            covid.Add(new Covid("CO01", "Covid", 2020));
            covid.Add(new Covid("CO02", "Gamma", 2021));
            covid.Add(new Covid("103", "Delta", 2023));
            covid.Add(new Covid("104", "Beta", 2022));
        }
    }
}

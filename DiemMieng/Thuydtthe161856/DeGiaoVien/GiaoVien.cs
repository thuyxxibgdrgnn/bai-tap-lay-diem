﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeGiaoVien
{
    internal class GiaoVien
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public double SoGioDay { get; set; }

        public GiaoVien()
        {
        }

        public GiaoVien(int iD, string ten, double soGioDay)
        {
            ID = iD;
            Ten = ten;
            SoGioDay = soGioDay;
        }
        public virtual void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}", this.ID, this.Ten, this.SoGioDay));
        }
    }
}

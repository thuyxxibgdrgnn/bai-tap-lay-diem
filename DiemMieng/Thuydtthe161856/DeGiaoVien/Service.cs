﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeGiaoVien
{
    internal class Service
    {
        Validate validate = new Validate();
        List<GiaoVien> giaoVien = new List<GiaoVien>();
        public void NhapDoiTuong()
        {
            do
            {
                int ID = validate.checkIdExist(giaoVien, "Nhập mã SV: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                double soGioDay = validate.CheckDouble("Nhập số giờ dạy: ");
                GiaoVien sv = new GiaoVien(ID, ten, soGioDay);
                giaoVien.Add(sv);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}",
                                            "ID", "Họ và tên", "Số giờ dạy"));
            foreach (GiaoVien gv in giaoVien)
            {
                gv.inThongTin();
            }

        }
        public void TimGiaoVienTheoSoGioDay(double soGio)
        {
            if (giaoVien.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            
            List<GiaoVien> list = new List<GiaoVien>();
            
            foreach (GiaoVien gv in giaoVien)
            {
                if (gv.SoGioDay.Equals(soGio))
                {
                    list.Add(gv);
                   
                }
            }
            if (list.Count==0)
            {
                Console.WriteLine("Không tìm thấy giáo viên này");
            }
            else {
                Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}",
                                             "ID", "Họ và tên", "Số giờ dạy"));
                for(int i = 0;i < list.Count; i++)
                {
                    list[i].inThongTin();
                }
            }

        }
        public void XoaDoiTuongTheoID(int ID)
        {
            if (giaoVien.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            bool check = false;

            for (int i = 0; i < giaoVien.Count; i++)
            {
                if (giaoVien[i].ID == ID)
                {
                    check = true;
                    giaoVien.RemoveAt(i);
                    break;
                }
            }

            if (!check)
            {
                Console.WriteLine("ID không tồn tại");
            }
            else
            {
                Console.WriteLine("Xóa thành công!");
            }
        }
        public void KeThua()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}{3, -10} ",
                                             "ID", "Họ và tên", "Số giờ dạy", "Ngành dạy"));
            GiaoVienPoly gvPoly = new GiaoVienPoly( "SE",789, "Nguyễn Thanh Vân", 205);
            gvPoly.inThongTin();
        }
        public void NhapDuLieu()
        {
            giaoVien.Add(new GiaoVien(123, "Đỗ Thanh Thủy", 2002));
            giaoVien.Add(new GiaoVien(234, "Nguyễn Duy Phúc", 2003));
            giaoVien.Add(new GiaoVien(345, "Nguyễn Cẩm Tú", 2003));
            giaoVien.Add(new GiaoVien(456, "Nguyễn Văn An", 1950));
        }
    }
}

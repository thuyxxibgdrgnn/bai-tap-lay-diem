﻿// See https://aka.ms/new-console-template for more information
using DeGiaoVien;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1.Nhập danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xuất danh sách GV có giờ dạy theo khoảng do người dùng nhập");
    Console.WriteLine("4.Xóa Đỗi tượng theo mã");
    Console.WriteLine("5.Kế thừa");
    Console.WriteLine("0.Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 5);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            double soGio = validate.CheckDouble("Nhập số giờ dạy của giáo viên cần tìm: ");
            service.TimGiaoVienTheoSoGioDay(soGio);
            break;
        case 4:
            int ID = validate.CheckInt("Nhập ID giáo viên cần xóa: ");
            service.XoaDoiTuongTheoID(ID);
            break;
        case 5:
            service.KeThua();
            break;
        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();

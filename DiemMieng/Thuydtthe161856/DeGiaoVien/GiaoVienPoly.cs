﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeGiaoVien
{
    internal class GiaoVienPoly: GiaoVien
    {
        public string NganhDay { get; set; }
        public GiaoVienPoly()
        {
        }
        public GiaoVienPoly(string nganhDay,int iD, string ten, double soGioDay) : base(iD, ten, soGioDay)
        {
            NganhDay = nganhDay;
        }
        public override void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}{3, -15}", this.ID, this.Ten, this.SoGioDay, this.NganhDay));
        }

    }
}

﻿// See https://aka.ms/new-console-template for more information
using DeMayTinh;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1.Nhập danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xóa đối tượng theo ID");
    Console.WriteLine("4.Xuất trọng lượng máy tính theo tên gần đúng");
    Console.WriteLine("0.Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
           service.NhapDoiTuong();  
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            string id = validate.CheckString("Nhập id đối tượng muốn xóa: ", "^[A-Za-z0-9]+$");
            service.XoaDoiTuongTheoID(id);
            break;
        case 4:
            string ten = validate.CheckString("Nhập tên máy tính muốn tìm: ", "^[A-Za-z0-9]+$");
            service.XuatTrongLuongTheoTenGanDung(ten);
            break;

        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();

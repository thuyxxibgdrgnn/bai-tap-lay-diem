﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DeMayTinh
{
    internal class Validate
    {
        public int CheckInt(string mess)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());

                    if (input < 0)
                    {
                        throw new Exception();
                    }

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Nhập số nguyên ");
                }
            } while (true);

            return input;
        }
        public float CheckFloat(string mess)
        {
            float input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());

                    if (input < 0)
                    {
                        throw new Exception();
                    }

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Không hợp lệ, hãy nhập lại: ");
                }
            } while (true);

            return input;
        }
        public string CheckString(string message, string pattern)
        {
            string input = string.Empty;
            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Không hợp lệ");
                    continue;
                }
                else
                {
                    break;
                }
            }

            return input;
        }

        public string checkNameExist(List<MayTinh> danhSach, string mess)
        {
            Console.Write(mess);
            string ten = "";
            do
            {
                ten = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(ten))
                {
                    Console.WriteLine("Mã SV rỗng");
                    Console.Write(mess);
                }
                else
                {
                    bool isDuplicate = false;
                    foreach (MayTinh mayTinh in danhSach)
                    {
                        if (ten.ToLower().Equals(mayTinh.Ten.ToLower()))
                        {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (isDuplicate)
                    {
                        Console.WriteLine("Tên đã tồn tại");
                        Console.Write(mess);
                    }
                    else
                    {
                        break;
                    }
                }
            } while (true);
            return ten;
        }
        public int checkIntLimit(string mess, int a, int b)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());
                    if (input >= a && input <= b)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Số nhập phải nằm trong khoảng " + a + "-" + b);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Lựa chọn không hợp lệ");
                }
            } while (true);
            return input;
        }
        public bool CheckYesNo(string message)
        {
            while (true)
            {
                Console.Write(message);
                string input = Console.ReadLine();
                if (input.ToLower().Equals("y"))
                {
                    return true;
                }
                else if (input.ToLower().Equals("n"))
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Nhập Y/y hoặc N/n!: ");
                }
            }
        }
    }
}

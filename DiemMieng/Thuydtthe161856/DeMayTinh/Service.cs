﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DeMayTinh
{
    internal class Service
    {
        Validate validate = new Validate();
        List<MayTinh> mayTinh = new List<MayTinh>();

        public int TuTangMa()
        {
            int tongMayTinh = mayTinh.Count();
            int maMayTinh = 1;

            if (tongMayTinh > 0)
            {
                maMayTinh = int.Parse(mayTinh[tongMayTinh - 1].ID) + 1;
            }

            return maMayTinh;
        }
        public void NhapDoiTuong()
        {
            do
            {
                string ID = ""+ TuTangMa();
                string ten = validate.checkNameExist(mayTinh,"Nhập tên: ");
                float trongLuong= validate.CheckFloat("Nhập trọng lượng: ");
                MayTinh mt = new MayTinh(ID, ten, trongLuong);
                mayTinh.Add(mt);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -10}",
                                            "ID", "Tên", "Trọng Lượng"));
            foreach (MayTinh mt in mayTinh)
            {
                mt.inThongTin();
            }

        }
        public void XoaDoiTuongTheoID(string ID)
        {
            if (mayTinh.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            bool check = false;
            if (String.IsNullOrEmpty(ID))
            {
                Console.WriteLine("ID rỗng");
                return;
            }
            for (int i = 0; i < mayTinh.Count; i++)
            {
                if (mayTinh[i].ID == ID)
                {
                    check = true;
                    mayTinh.RemoveAt(i);
                    break;
                }
            }

            if (!check)
            {
                Console.WriteLine("ID không tồn tại");
            }
            else
            {
                Console.WriteLine("Xóa thành công!");
            }
        }
        public void XuatTrongLuongTheoTenGanDung(string ten)
        {
            if (mayTinh.Count == 0)
            {
                Console.WriteLine("Không có dữ liệu");
                return;
            }
            if (string.IsNullOrWhiteSpace(ten))
            {
                Console.WriteLine("Tên rỗng");
                return;
            }
            List<MayTinh> list = new List<MayTinh>();
            foreach (MayTinh mt in mayTinh)
            {
                if (mt.Ten.ToUpper().Contains(ten.ToUpper()))
                {
                    list.Add(mt);
                }
            }

            if (list.Count == 0)
            {
                Console.WriteLine("Không tìm thấy");
                
            }
            else
            {
                Console.WriteLine(string.Format("{0, -15}{1, -10}",
                                                "Tên", "Trọng Lượng"));
               
                foreach (MayTinh mt in list)
                {
                    mt.inTrongLuong();
                }
            }
            Console.WriteLine();
        }
        public void NhapDuLieu()
        {
            mayTinh.Add(new MayTinh("1", "Dell", 1.8f));
            mayTinh.Add(new MayTinh("2", "Asus", 1.5f));
            mayTinh.Add(new MayTinh("3", "Lenovo", 2f));
            mayTinh.Add(new MayTinh("4", "Mac", 1.2f));
        }
    }
}

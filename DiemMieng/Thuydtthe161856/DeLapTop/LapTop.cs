﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeLapTop
{
    internal class LapTop
    {
        
        public int ID { get; set; }
        public string MaLaptop { get; set; }
        public double KichThuocMH { get; set; }
        public LapTop()
        {
        }

        public LapTop(int iD, string maLaptop, double kichThuocMH)
        {
            ID = iD;
            MaLaptop = maLaptop;
            KichThuocMH = kichThuocMH;
        }
        public void inThongTin()
        {

            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -15}", this.ID, this.MaLaptop, this.KichThuocMH));
        }
        public void inKichthuocMH()
        {

            Console.WriteLine(string.Format("{0, -15}{1, -20}", this.MaLaptop, this.KichThuocMH));
        }
    }
}

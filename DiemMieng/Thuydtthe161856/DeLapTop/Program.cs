﻿
using DeLapTop;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1.Nhập danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xóa đối tượng theo ID");
    Console.WriteLine("4.Xuất kích thước màn hình theo khoảng người dùng nhập vào");
    Console.WriteLine("0.Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
           int id= validate.CheckInt("Nhập ID laptop muốn xóa: ");
            service.XoaDoiTuongTheoID(id);
            break;
        case 4:
            service.XuatKichThuocManHinhTheoKhoangNhap();
            break;

        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();

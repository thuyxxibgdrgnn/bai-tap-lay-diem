﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeLapTop
{
    internal class Service
    {
        Validate validate = new Validate();
        List<LapTop> lapTop = new List<LapTop>();
        
        public void NhapDoiTuong()
        {
            do
            {
                int ID = validate.checkIdExist(lapTop, "Nhập ID: ");
                string maLapTop = validate.CheckString("Nhập mã máy tính: ", "^[A-Za-z0-9]+$");
                double kichThuocMH = validate.CheckDouble("Nhập kích thước màn hình: ");
                LapTop lt = new LapTop (ID, maLapTop, kichThuocMH);
                lapTop.Add(lt);
                //lapTop = lapTop.OrderBy(lt => lt.KichThuocMH).ToList();
                SapXepDanhSachTheoKichThuoc();

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -15}",
                                            "ID", "Mã laptop", "Kích thước màn hình"));
            foreach (LapTop lt in lapTop)
            {
                lt.inThongTin();
            }

        }
        public void XoaDoiTuongTheoID(int ID)
        {
            if (lapTop.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            bool check = false;

            for (int i = 0; i < lapTop.Count; i++)
            {
                if (lapTop[i].ID == ID)
                {
                    check = true;
                    lapTop.RemoveAt(i);
                    break;
                }
            }

            if (!check)
            {
                Console.WriteLine("ID không tồn tại");
            }
            else
            {
                Console.WriteLine("Xóa thành công!");
            }
        }
        public void XuatKichThuocManHinhTheoKhoangNhap()
        {
            Console.WriteLine("Nhập khoảng a - b");
            double a = validate.CheckDouble("Nhập a:");
            double b = validate.CheckDouble("Nhập b: ");
            if(lapTop.Count == 0)
            {
                Console.WriteLine("Không có dữ liệu");
            }
            List<LapTop> list = new List<LapTop>();
            foreach (LapTop lt in lapTop)
            {
                if (lt.KichThuocMH >= a && lt.KichThuocMH <= b)
                {
                    list.Add(lt);
                }
            }
            if(list.Count == 0)
            {
                Console.WriteLine("Không tìm thấy laptop nào thỏa mãn điều kiện");
            }
            else {
                Console.WriteLine(string.Format("{0, -15}{1, -20}","Mã LapTop", "Kích thước màn hình"));
                foreach (LapTop lt in list)
                {
                    lt.inKichthuocMH();
                }
            }
        }
        public void SapXepDanhSachTheoKichThuoc()
        {
            for (int i = 0; i < lapTop.Count - 1; i++)
            {
                for (int j = i + 1; j < lapTop.Count; j++)
                {
                    if (lapTop[i].KichThuocMH > lapTop[j].KichThuocMH)
                    {
                       
                        LapTop temp = lapTop[i];
                        lapTop[i] = lapTop[j];
                        lapTop[j] = temp;
                    }
                }
            }
        }
        public void NhapDuLieu()
        {
            lapTop.Add(new LapTop(1, "Dell", 13f));
            lapTop.Add(new LapTop(2, "Asus", 14.6f));
            lapTop.Add(new LapTop(3, "Lenovo",15.6f));
            lapTop.Add(new LapTop(4, "Mac", 14f));
            //lapTop = lapTop.OrderBy(lt => lt.KichThuocMH).ToList();
            SapXepDanhSachTheoKichThuoc();
        }

    }
}

﻿// See https://aka.ms/new-console-template for more information
using DeHocSinh;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách học sinh");
    Console.WriteLine("2. Xuất danh sách học sinh");
    Console.WriteLine("3. Xuất danh sách học sinh có năm sinh");
    Console.WriteLine("4. Xóa học sinh theo Mã HS");
    Console.WriteLine("0. Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            service.XuatDanhSachHocSinhCoNamSinh();
            break;
        case 4:
            string ma = validate.CheckString("Nhập mã sinh viên cần tìm: ", "^[A-Za-z0-9]+$");
            service.XoaDoiTuongTheoID(ma);
            break;
        
        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();

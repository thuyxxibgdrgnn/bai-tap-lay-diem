﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeHocSinh
{
    internal class HocSinh
    {
        public string MaHs { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }

        public HocSinh(string maHs, string ten, int tuoi)
        {
            MaHs = maHs;
            Ten = ten;
            Tuoi = tuoi;
        }
        public virtual void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -30}{2, -10}", this.MaHs, this.Ten, this.Tuoi));
        }
    }
}

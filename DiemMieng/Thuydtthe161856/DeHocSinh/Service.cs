﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeHocSinh
{
    internal class Service
    {
        List<HocSinh> danhSachHocSinh = new List<HocSinh>();
        Validate validate = new Validate();
        public void NhapDoiTuong()
        {
            do
            {
                string maHS = validate.checkIdExist(danhSachHocSinh, "Nhập mã học sinh: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int tuoi = validate.CheckInt("Nhập năm sinh: ");
                HocSinh hs = new HocSinh(maHS, ten, tuoi);
                danhSachHocSinh.Add(hs);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -30}{2, -10}",
                                            "Mã SV", "Họ và tên", "Tuổi"));
            foreach (HocSinh sv in danhSachHocSinh)
            {
                sv.inThongTin();
            }

        }
        public void XuatDanhSachHocSinhCoNamSinh()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -30}{2, -10}{3, -10}",
                                            "Mã SV", "Họ và tên","Tuổi", "Năm sinh"));
            foreach (var hocSinh in danhSachHocSinh)
            {
                Console.WriteLine(string.Format("{0, -15}{1, -30}{2, -10}{3, -10}", hocSinh.MaHs, hocSinh.Ten,hocSinh.Tuoi, DateTime.Now.Year - hocSinh.Tuoi));
            }
        }
        public void XoaDoiTuongTheoID(string ID)
        {
            if (danhSachHocSinh.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            bool check = false;

            for (int i = 0; i < danhSachHocSinh.Count; i++)
            {
                if (danhSachHocSinh[i].MaHs.ToLower() == ID.ToLower())
                {
                    check = true;
                    danhSachHocSinh.RemoveAt(i);
                    break;
                }
            }

            if (!check)
            {
                Console.WriteLine("ID không tồn tại");
            }
            else
            {
                Console.WriteLine("Xóa thành công!");
            }
        }
        public void NhapDuLieu()
        {
            danhSachHocSinh.Add(new HocSinh("123","Đỗ Thanh Thủy",12));
            danhSachHocSinh.Add(new HocSinh("234", "Nguyễn Phương Linh", 15));
            danhSachHocSinh.Add(new HocSinh("345", "Tạ Văn Minh", 13));
            danhSachHocSinh.Add(new HocSinh("456", "Nguyễn Minh", 14));
        }
    }
}

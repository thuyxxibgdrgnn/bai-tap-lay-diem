﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeNganhHoc
{
    internal class NganhHoc
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int SoKyHoc {  get; set; }

        public NganhHoc()
        {
        }

        public NganhHoc(int iD, string name, int soKyHoc)
        {
            ID = iD;
            Name = name;
            SoKyHoc = soKyHoc;
        }
        public void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}", this.ID, this.Name, this.SoKyHoc));
        }
    }
}

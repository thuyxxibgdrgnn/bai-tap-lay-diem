﻿// See https://aka.ms/new-console-template for more information
using DeNganhHoc;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất các ngành học có kỳ học lớn hơn 6");
    Console.WriteLine("4. Xóa kỳ học theo ID nhập vào");
    Console.WriteLine("0. Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            service.TimNganhDayTren6();
            break;
        case 4:
            int ma = validate.CheckInt("Nhập mã muốn xóa: ");
            service.XoaDoiTuongTheoID(ma);
            break;
        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();
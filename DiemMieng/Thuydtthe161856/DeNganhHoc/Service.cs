﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeNganhHoc
{
    internal class Service
    {
        Validate validate = new Validate();
        List<NganhHoc> dsNganhHoc = new List<NganhHoc>();
        public void NhapDoiTuong()
        {
            do
            {
                int ID = validate.checkIdExist(dsNganhHoc, "Nhập mã: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int soKyHoc = validate.CheckInt("Nhập số ngành học: ");
                NganhHoc nganh = new NganhHoc(ID, ten, soKyHoc);
                dsNganhHoc.Add(nganh);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}",
                                            "ID", "Họ và tên", "Số giờ dạy"));
            foreach (NganhHoc nganh in dsNganhHoc)
            {
                nganh.inThongTin();
            }

        }
        public void TimNganhDayTren6()
        {
            if (dsNganhHoc.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }

            List<NganhHoc> list = new List<NganhHoc>();

            foreach (NganhHoc nganh in dsNganhHoc)
            {
                if (nganh.SoKyHoc>=6)
                {
                    list.Add(nganh);

                }
            }
            if (list.Count == 0)
            {
                Console.WriteLine("Không tìm thấy");
            }
            else
            {
                Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}",
                                             "ID", "Tên", "Số ngành học"));
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].inThongTin();
                }
            }

        }
        public void XoaDoiTuongTheoID(int ID)
        {
            if (dsNganhHoc.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            bool check = false;

            for (int i = 0; i < dsNganhHoc.Count; i++)
            {
                if (dsNganhHoc[i].ID == ID)
                {
                    check = true;
                    dsNganhHoc.RemoveAt(i);
                    break;
                }
            }

            if (!check)
            {
                Console.WriteLine("ID không tồn tại");
            }
            else
            {
                Console.WriteLine("Xóa thành công!");
            }
        }
        
        public void NhapDuLieu()
        {
            dsNganhHoc.Add(new NganhHoc(123, "Marketing", 5));
            dsNganhHoc.Add(new NganhHoc(234, "Ngôn ngữ Anh", 4));
            dsNganhHoc.Add(new NganhHoc(345, "Software Engineering", 6));
            dsNganhHoc.Add(new NganhHoc(456, "AI", 9));
        }
    }
}

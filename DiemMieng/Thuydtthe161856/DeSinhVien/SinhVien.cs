﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeSinhVien
{
    internal class SinhVien
    {
        public string MaSV { get; set; }
        public string Ten { get; set; }
        public int NamSinh { get; set; }

        public SinhVien()
        {
        }

        public SinhVien(string maSV, string ten, int namSinh)
        {
            MaSV = maSV;
            Ten = ten;
            NamSinh = namSinh;
        }
        public virtual void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -20}{2, -10}", this.MaSV, this.Ten, this.NamSinh));
        }
    }
}

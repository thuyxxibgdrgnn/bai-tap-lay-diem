﻿// See https://aka.ms/new-console-template for more information
using DeSinhVien;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1.Nhập danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xuất danh sách các SV tuổi từ 50 trở lên");
    Console.WriteLine("4.Tìm SV theo mã");
    Console.WriteLine("5.Kế thừa");
    Console.WriteLine("0.Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0,5);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
           service.DanhSachSVTRen50Tuoi();
            break;
        case 4:
            string ma = validate.CheckString("Nhập mã sinh viên cần tìm: ", "^[A-Za-z0-9]+$");
            service.TimSinhVien(ma);
            break;
        case 5:
            service.KeThua();
            break;
        default:
            break;
    }
} while (choice!=0);
Console.ReadKey();



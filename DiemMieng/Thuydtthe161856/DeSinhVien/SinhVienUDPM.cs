﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeSinhVien
{
    internal class SinhVienUDPM : SinhVien
    {
        public double DiemJava {  get; set; }
        public double DiemCsharp { get; set; }

        public SinhVienUDPM()
        {
        }

        public SinhVienUDPM(double diemJava, double diemCsharp)
        {
            DiemJava = diemJava;
            DiemCsharp = diemCsharp;
        }

        public SinhVienUDPM(string maSV, string ten, int namSinh, double diemJava, double diemCsharp) : base(maSV, ten, namSinh)
        {
            DiemJava = diemJava;
            DiemCsharp = diemCsharp;
        }
        public virtual void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -20}{2, -10}{3,-15}{4,-15}", this.MaSV, this.Ten, this.NamSinh, this.DiemJava, this.DiemCsharp));
        }
    }
}

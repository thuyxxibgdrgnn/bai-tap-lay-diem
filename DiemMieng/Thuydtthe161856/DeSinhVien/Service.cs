﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace DeSinhVien
{
    internal class Service
    {
        Validate validate = new Validate();
        List<SinhVien> sinhVien = new List<SinhVien>();
        
        public void NhapDoiTuong()
        {
            do
            {
                string maSV = validate.checkIdExist(sinhVien, "Nhập mã SV: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int namSinh = validate.CheckInt("Nhập năm sinh: ");
                SinhVien sv = new SinhVien(maSV, ten, namSinh);
                sinhVien.Add(sv);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -20}{2, -10}",
                                            "Mã SV", "Họ và tên", "Năm Sinh"));
            foreach (SinhVien sv in sinhVien)
            {
                sv.inThongTin();
            }

        }
        public void DanhSachSVTRen50Tuoi()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -20}{2, -10}",
                                           "Mã SV", "Họ và tên", "Năm Sinh"));
            foreach (SinhVien sv in sinhVien)
            {
                if (DateTime.Now.Year - sv.NamSinh > 50)
                {
                    sv.inThongTin();
                }
               
            }
        }
        public void TimSinhVien(string ma)
        {
            if (sinhVien.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }

            if (string.IsNullOrWhiteSpace(ma))
            {
                Console.WriteLine("Mã rỗng");
                return;
            }
            bool check = false;
            foreach (SinhVien sv in sinhVien)
            {
                if (sv.MaSV.Equals(ma))
                {
                    Console.WriteLine(string.Format("{0, -15}{1, -20}{2, -10}",
                                             "Mã SV", "Họ và tên", "Năm Sinh"));
                    sv.inThongTin();
                    check = true;
                }
            }
            if (check == false)
            {
                Console.WriteLine("Không tìm thấy sinh viên này");
            }
        }
        public void KeThua()
        {
            Console.WriteLine(string.Format("{0, -15}{1, -20}{2, -10}{3,-15}{4,-15}",
                                             "ID", "Họ và tên", "Năm Sinh", "Điểm Java", "Điểm C#"));
            SinhVienUDPM sv = new SinhVienUDPM( "678", "Nguyễn Thanh Vân", 2005, 7f, 6.7f);
            sv.inThongTin();
        }
        public void NhapDuLieu()
        {
            sinhVien.Add(new SinhVien("123", "Đỗ Thanh Thủy", 2002));
            sinhVien.Add(new SinhVien("234", "Nguyễn Duy Phúc", 2003));
            sinhVien.Add(new SinhVien("345", "Nguyễn Cẩm Tú", 2003));
            sinhVien.Add(new SinhVien("456", "Nguyễn Văn An", 1950));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeBike
{
    internal class Bike
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public string HSX { get; set; }

        public Bike(int id, string ten, string hsx)
        {
            ID = id;
            Ten = ten;
            HSX = hsx;
        }
        public void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}", this.ID, this.Ten, this.HSX));
        }
    }
}

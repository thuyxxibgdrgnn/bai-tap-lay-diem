﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeBike
{
    internal class Service
    {
        Validate validate = new Validate();
        List<Bike> bikeList = new List<Bike>();
        public void NhapDoiTuong()
        {
            do
            {
                int ID = validate.checkIdExist(bikeList, "Nhập ID: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                string hangSX = validate.CheckString("Nhập hãng sản xuất: ", "^[a-zA-ZÀ-ỹ\\0-9]+$");
                Bike b = new Bike(ID, ten, hangSX);
                bikeList.Add(b);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}",
                                            "ID", "Tên", "Hãng sản xuất"));
            foreach (Bike b in bikeList)
            {
                b.inThongTin();
            }

        }
        public void XuatXeMayHangHONDA()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}",
                                            "ID", "Tên", "Hãng sản xuất"));
            foreach (var bike in bikeList)
            {
                if (bike.HSX.ToUpper().Equals("HONDA"))
                {
                    bike.inThongTin();
                }
            }
        }
        public void SapXepTheoIDGiamDan()
        {
            for (int i = 0; i < bikeList.Count - 1; i++)
            {
                for (int j = i + 1; j < bikeList.Count; j++)
                {
                    if (bikeList[i].ID < bikeList[j].ID)
                    {
                        Bike temp = bikeList[i];
                        bikeList[i] = bikeList[j];
                        bikeList[j] = temp;
                    }
                }
            }
            HienThiDanhSach();
        }
        public void NhapDuLieu()
        {
            bikeList.Add(new Bike(1, "Air Blade", "Honda"));
            bikeList.Add(new Bike(2, "INNOVA", "Toyota"));
            bikeList.Add(new Bike(3, "Xe Điện", "Vinfast"));
            bikeList.Add(new Bike(4, "Wave Alpha", "Honda"));
            bikeList.Add(new Bike(5, "Lead", "Honda"));
        }

    }
}

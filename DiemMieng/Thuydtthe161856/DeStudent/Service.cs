﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeStudent
{
    internal class Service
    {
        Validate validate = new Validate();
        List<Student> studentList = new List<Student>();
        public void NhapDoiTuong()
        {
            do
            {
                int ID = validate.checkIdExist(studentList, "Nhập mã SV: ");
                string ten = validate.CheckString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int tuoi = validate.CheckInt("Nhập số giờ dạy: ");
                string nganh = validate.CheckString("Nhập ngành học: ", "^[a-zA-ZÀ-ỹ\\0-9]+$");
                Student sv = new Student(ID, ten, tuoi, nganh);
                studentList.Add(sv);

            } while (validate.CheckYesNo("Bạn có muốn tiếp tục thêm không?(Y/N): "));

        }
        public void HienThiDanhSach()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}{3, -15}",
                                            "ID", "Họ và tên", "Tuổi","Ngành học"));
            foreach (Student student in studentList)
            {
                student.inThongTin();
            }

        }
        public void XuatDanhSachHocSinhCoNamSinh()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}{3, -15}{4, -10}",
                                            "Mã SV", "Họ và tên", "Tuổi","Ngành học", "Năm sinh"));
            foreach (var student in studentList)
            {
                Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}{3, -15}{4, -10}", student.ID, student.Ten, student.Tuoi,student.Nganh, DateTime.Now.Year - student.Tuoi));
            }
        }
        public void XoaDoiTuongTheoID(int ID)
        {
            if (studentList.Count == 0)
            {
                Console.WriteLine("Danh sách trống");
                return;
            }
            bool check = false;

            for (int i = 0; i < studentList.Count; i++)
            {
                if (studentList[i].ID == ID)
                {
                    check = true;
                    studentList.RemoveAt(i);
                    break;
                }
            }

            if (!check)
            {
                Console.WriteLine("ID không tồn tại");
            }
            else
            {
                Console.WriteLine("Xóa thành công!");
            }
        }
        public void NhapDuLieu()
        {
            studentList.Add(new Student(123, "Đỗ Thanh Thủy", 20,"SE"));
            studentList.Add(new Student(234, "Nguyễn Phương Linh", 21, "AI"));
            studentList.Add(new Student(345, "Tạ Văn Minh", 19, "Marketing"));
            studentList.Add(new Student(456, "Nguyễn Minh", 22 , "SE"));
        }
    }
}

﻿// See https://aka.ms/new-console-template for more information

using DeStudent;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Service service = new Service();
service.NhapDuLieu();

do
{
    Console.WriteLine("===========Menu=============");
    Console.WriteLine("1. Nhập danh sách sinh viên");
    Console.WriteLine("2. Xuất danh sách sinh viên");
    Console.WriteLine("3. Xuất danh sách sinh viên có năm sinh");
    Console.WriteLine("4. Xóa sinh viên theo ID");
    Console.WriteLine("0. Thoát");
    choice = validate.checkIntLimit("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDoiTuong();
            break;
        case 2:
            service.HienThiDanhSach();
            break;
        case 3:
            service.XuatDanhSachHocSinhCoNamSinh();
            break;
        case 4:
            int ma = validate.CheckInt("Nhập mã sinh viên cần tìm: ");
            service.XoaDoiTuongTheoID(ma);
            break;

        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeStudent
{
    internal class Student
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string Nganh { get; set; }

        public Student(int id, string ten, int tuoi, string nganh)
        {
            ID = id;
            Ten = ten;
            Tuoi = tuoi;
            Nganh = nganh;
        }

        public void inThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -20}{3, -15}", this.ID, this.Ten, this.Tuoi, this.Nganh));
        }
    }
}
